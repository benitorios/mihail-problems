package info.benitorios.mihail.problems.checkers;

import java.time.Duration;

public class Checkers {
	
	private static final int MAX_NEIGHBOUR_COUNT = 2;
	private static final int SQUARES_COUNT = Board.DIMENSION * Board.DIMENSION;
	private static final int MIN_HEURISTIC_POINTS = 43;
	
	// In which row is every square. More efficient than this: row = (square - 1) / Board.DIMENSION;
	private static final int[] ROW_FOR_SQUARES = new int[] {
		0, 0, 0, 0, 0, 0, 0, 0,
		1, 1, 1, 1, 1, 1, 1, 1,
		2, 2, 2, 2, 2, 2, 2, 2,
		3, 3, 3, 3, 3, 3, 3, 3,
		4, 4, 4, 4, 4, 4, 4, 4,
		5, 5, 5, 5, 5, 5, 5, 5,
		6, 6, 6, 6, 6, 6, 6, 6,
		7, 7, 7, 7, 7, 7, 7, 7
	};
	
	// In which col is every square. More efficient than this: col = (square - 1) % Board.DIMENSION;
	private static final int[] COL_FOR_SQUARES = new int[] {
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7,
		0, 1, 2, 3, 4, 5, 6, 7
	};
	
	
	private long attemptCount = 0;
	private long prunedBranches = 0;
	private int bestCheckerCount = 0;

	public Board findBoard() {
		return findBoard(new Board(), 1);
	}
	
	public Board findBoard(Board board, int fromSquare) {
		if (fromSquare > SQUARES_COUNT) {
			int checkerCount = board.checkerCount();
			bestCheckerCount = Math.max(bestCheckerCount, checkerCount);
			attemptCount++;
			if ((attemptCount & 0xffffff00) == attemptCount) { // Don't show all attempts -> IO operations are costly!
				System.out.printf("Attempt #%d -> checkers count: %d; best result until now: %d; braches pruned: %d\n%s\n",
						attemptCount, checkerCount, bestCheckerCount, prunedBranches, board);
			}
			return board;
		}
		
		// Heuristic
		if (heuristicPoints(board, fromSquare) < MIN_HEURISTIC_POINTS) {
			// prune this branch - solution good enough not possible from here;
			prunedBranches++;
			return null;
		}
		
		// int fromRow = (fromSquare - 1) / Board.DIMENSION;
		// int fromCol = (fromSquare - 1) % Board.DIMENSION;
		// This is more efficient:
		int fromRow = ROW_FOR_SQUARES[fromSquare - 1];
		int fromCol = COL_FOR_SQUARES[fromSquare - 1];
		boolean allowedCheckerHere = allowedCheckerInSquare(board, fromRow, fromCol);
		if (allowedCheckerHere) {
			// Try placing a checker
			Board copy = board.copy();
			copy.setAt(fromRow, fromCol);
			Board result1 = findBoard(copy, fromSquare + 1);
			
			// Try without placing a checker;
			Board result2 = findBoard(board, fromSquare + 1);
			
			if (result1 == null && result2 == null) {
				return null;
			}
			if (result1 != null && result2 != null) {
				int checkersCount1 = result1.checkerCount();
				int checkersCount2 = result2.checkerCount();
				return checkersCount1 >= checkersCount2 ? result1 : result2;
			}
			return result1 != null ? result1 : result2;
		} else {
			return findBoard(board, fromSquare + 1);
		}
	}

	private int heuristicPoints(Board board, int fromSquare) {
		int checkersCount = board.checkerCount();
		return checkersCount + SQUARES_COUNT - fromSquare + 1; 
	}
	
	private boolean allowedCheckerInSquare(Board board, int row, int col) {
		if (row > 0 && board.getAt(row - 1, col)) {
			int topCheckerNeighbours = countNeighboursAt(board, row - 1, col);
			if (topCheckerNeighbours >= MAX_NEIGHBOUR_COUNT) {
				return false;
			}
		}
		
		if (col > 0 && board.getAt(row, col - 1)) {
			int leftCheckerNeighbours = countNeighboursAt(board, row, col - 1);
			if (leftCheckerNeighbours >= MAX_NEIGHBOUR_COUNT) {
				return false;
			}
		}
		
		return true;
	}
	
	private int countNeighboursAt(Board board, int row, int col) {
		int count = 0;
		// Top
		if (row > 0) {
			count += board.getAt(row - 1, col) ? 1 : 0;
		}
		// Right
		if (col < Board.DIMENSION - 1) {
			count += board.getAt(row, col + 1) ? 1 : 0;
		}
		// Bottom
		if (row < Board.DIMENSION - 1) {
			count += board.getAt(row + 1, col) ? 1 : 0;
		}
		// Left
		if (col > 0) {
			count += board.getAt(row, col - 1) ? 1 : 0;
		}
		
		return count;
	}
	
	public static void main(String[] args) {
		System.out.println("Searching...");
		
		long start = System.currentTimeMillis();
		
		Checkers c = new Checkers();
		Board board = c.findBoard();
		
		long end = System.currentTimeMillis();
		
		System.out.printf("Best solution found:\n%s\n", board);
		System.out.printf("Checkers count: %d\n", board.checkerCount());
		
		Duration timeNeeded = Duration.ofMillis(end - start);
		System.out.printf("Time needed: %s\n", timeNeeded);
	}
}