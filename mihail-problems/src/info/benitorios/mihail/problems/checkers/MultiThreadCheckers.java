package info.benitorios.mihail.problems.checkers;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MultiThreadCheckers {

	private final ExecutorService executor;
	private final int poolSize;
	private int startingSquare;
	
	public MultiThreadCheckers(int poolSize) {
		checkPowerOfTwo(poolSize);
		this.poolSize = poolSize;
		executor = Executors.newFixedThreadPool(poolSize);
	}
	
	private void checkPowerOfTwo(int n) {
		int nearestPowerOfTwo = nearestPowerOfTwo(n);
		if (nearestPowerOfTwo != n) {
			throw new IllegalArgumentException(n + " is not a power of 2");
		}
	}
		
	public void run() throws Exception {
		long start = System.currentTimeMillis();
		Board[] boards = createInitialBoardsAndSetStaringSquare();
		showStartingMessage(boards);
		Collection<? extends Callable<Board>> tasks = createTasks(boards);
		try {
			List<Future<Board>> futures = executor.invokeAll(tasks);
			long end = System.currentTimeMillis();
			showResults(futures);
			Duration timeNeeded = Duration.ofMillis(end - start);
			System.out.printf("Time needed: %s\n", timeNeeded);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void showStartingMessage(Board[] boards) {
		System.out.println("Starting process...");
		System.out.printf("Tasks count: %d\n" , poolSize);
		for (int i = 0; i < boards.length; i++) {
			System.out.printf("Task #%d starts with board\n%s\n", i + 1, boards[i]);
		}
		System.out.printf("Starting square: %d\n" , startingSquare);
	}
	
	private int nearestPowerOfTwo(int n) {
		int power = 0;
		while (n > 1) {
			n /= 2;
			power++;
		}
		return pow(2, power);
	}
	
	private int pow(int n, int power) {
		int result = 1;
		for (int i = 0; i < power; i++) {
			result *= n;
		}
		return result;
	}
	
	private Board[] createInitialBoardsAndSetStaringSquare() {
		Board[] boards = new Board[poolSize];
		for (int i = 0; i < poolSize; i++) {
			boards[i] = createInitialBoardAndSetStartingSquare(i);
		}
		return boards;
	}
	
	private Board createInitialBoardAndSetStartingSquare(int n) {
		Board board = new Board();
		this.startingSquare = 1;
		for (int row = 0; row < Board.DIMENSION && n > 0; row++) {
			for (int col = 0; col < Board.DIMENSION && n > 0; col++) {
				int b = n % 2;
				if (b == 1) {
					board.setAt(row, col);
				}
				n /= 2;
				startingSquare++;
			}
		}
		return board;
	}
	
	private Collection<? extends Callable<Board>> createTasks(Board[] boards) {
		Collection<FindBoardTask> tasks = new ArrayList<>();
		for (Board board : boards) {
			FindBoardTask task = new FindBoardTask(board, startingSquare);
			tasks.add(task);
		}
		return tasks;
	}
	
	private void showResults(List<Future<Board>> futures) throws Exception {
		System.out.println("******************************** Results ********************************\n");
		int resultNumber = 0;
		for (Future<Board> future : futures) {
			resultNumber++;
			Board board = future.get();
			System.out.printf("Result #%d: Checker count: %d; Board:\n%s\n\n", resultNumber, board.checkerCount(), board);
		}
	}
	
	private static final class FindBoardTask implements Callable<Board> {
		private final Board board;
		private final int startingSquare;
		
		public FindBoardTask(Board board, int startingSquare) {
			this.board = board;
			this.startingSquare = startingSquare;
		}
		
		@Override
		public Board call() throws Exception {
			Checkers c = new Checkers();
			return c.findBoard(board, startingSquare);
		}
	}
	
	public static void main(String[] args) throws Exception {
		MultiThreadCheckers e = new MultiThreadCheckers(4);
		e.run();
	}
}
