package info.benitorios.mihail.problems.checkers;

public class Board {
	
	public static final int DIMENSION = 8;
	
	private final boolean[][] contents = new boolean[DIMENSION][DIMENSION];
	private int checkerCount = 0;
	
	public void setAt(int row, int col) {
		if (!contents[row][col]) {
			contents[row][col] = true;
			checkerCount++;
		}
	}
	
	public boolean getAt(int row, int col) {
		return contents[row][col];
	}
	
	public int checkerCount() {
		return checkerCount;
	}
	
	public Board copy() {
		Board copy = new Board();
		for (int row = 0; row < DIMENSION; row++) {
			for (int col = 0; col < DIMENSION; col++) {
				copy.contents[row][col] = contents[row][col];
			}
		}
		copy.checkerCount = checkerCount;
		return copy;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int row = 0; row < DIMENSION; row++) {
			for (int col = 0; col < DIMENSION; col++) {
				builder.append(contents[row][col] ? "* " : "- ");
			}
			builder.append("\n");
		}
		return builder.toString();
	}
}
