package info.benitorios.mihail.problems.candies;

import java.util.HashMap;
import java.util.Map;

public class Candies {
	
	public int countCombinations(int amount, int[] candyPrices) {
		Map<Integer, Integer> combinations = new HashMap<>();
		combinations.put(0, 1); // For an amount of 0, we know that the number of solutions is 1.
		return countCombinations(amount, candyPrices, combinations);
	}
	
	public int countCombinations(int amount, int[] candyPrices, Map<Integer, Integer> knownSolutions) {				
		if (knownSolutions.get(amount) != null) {
			return knownSolutions.get(amount);
		}
		
		if (amount == 0) {
			knownSolutions.put(1,  1);
			return 1;
		}
		
		int combinations = 0;				
		for (int candyPrice : candyPrices) {
			if (candyPrice <= amount) {
				int subCombinations = countCombinations(amount - candyPrice, candyPrices, knownSolutions);
				combinations += subCombinations;
				knownSolutions.put(amount, combinations);
			}
		}
		
		return combinations;
	}
	
	public static void main(String[] args) {
		System.out.println("*** Candies Problem ***");
		System.out.println("Searching for combinations....");
		Map<Integer, Integer> combinations = new HashMap<>();
		combinations.put(0, 1); // For an amount of 0, we know that the number of solutions is 1.
		new Candies().countCombinations(40, new int[] {1, 2, 5, 10}, combinations);
		for (int amount = 0; amount <= 40; amount++) {
			System.out.printf("Combinations for amount %d -> %d\n", amount, combinations.get(amount));
		}
	}
}
