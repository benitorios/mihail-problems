package info.benitorios.mihail.problems.candies;

import static org.junit.Assert.*;

import org.junit.Test;

public class CandiesTest {
	
	private int[][] expectedResults = {
			// Amount			Combinations
			{		0, 					1},
			{		1,					1},
			{		2,					2},
			{		3,					3},
			{		4,					5},
			{		5,					9}
	};
	
	private int[] candyPrices = {1, 2, 5, 10};

	@Test
	public void test() {
		Candies underTest = new Candies();
		for (int[] expectedResult: expectedResults) {
			int actualResult = underTest.countCombinations(expectedResult[0], candyPrices);
			assertEquals(expectedResult[1], actualResult);
		}
	}

}
