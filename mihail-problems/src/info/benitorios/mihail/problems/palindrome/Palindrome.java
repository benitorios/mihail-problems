package info.benitorios.mihail.problems.palindrome;

public class Palindrome {
	
	enum Direction {
		LeftToRight,
		RightToLeft
	}

	public String findSmallestPalindrome(String word) {
		word = word.toLowerCase();
		
		// Buffers to contain the result
		StringBuilder leftPart = new StringBuilder();
		StringBuilder center = new StringBuilder();
		
		findSmallestPalindrome(word, 0, word.length(), leftPart, center);
		
		// Result
		return leftPart.toString() + center.toString() + leftPart.reverse().toString();
	}
	
	/*
	 * Recursively, tries to find the palindrome going from LeftToRight and RightToLeft,
	 * and takes the smallest result and discard the other.
	 */
	private void findSmallestPalindrome(String word, int begin, int end, StringBuilder leftPart, StringBuilder center) {
		StringBuilder leftPartLtoR = new StringBuilder();
		StringBuilder centerLtoR = new StringBuilder();
		findSmallestPalindrome(word, begin, end, leftPartLtoR, centerLtoR, Direction.LeftToRight);
		
		StringBuilder leftPartRtoL = new StringBuilder();
		StringBuilder centerRtoL = new StringBuilder();
		findSmallestPalindrome(word, begin, end, leftPartRtoL, centerRtoL, Direction.RightToLeft);
		
		if (leftPartLtoR.length() + centerLtoR.length() <= leftPartRtoL.length() + centerRtoL.length()) {
			// The palindrome found with the method Left-To-Right is smaller or same size
			leftPart.append(leftPartLtoR);
			center.append(centerLtoR);
		} else {
			// The palindrome found with the method Right-To-Left is smaller
			leftPart.append(leftPartRtoL);
			center.append(centerRtoL);
		}
	}
	
	private void findSmallestPalindrome(String word, int begin, int end, StringBuilder leftPart, StringBuilder center, Direction direction) {
		if (begin < end - 1) {
			char l = word.charAt(begin);
			char r = word.charAt(end - 1);
			char c = direction == Direction.LeftToRight ? l : r;
			leftPart.append(c);
			if (l == r) {
				// Move both indexes to the center
				findSmallestPalindrome(word, begin + 1, end - 1, leftPart, center);
			} else {
				if (direction == Direction.LeftToRight) {
					// Move just the left index to the center
					findSmallestPalindrome(word, begin + 1, end, leftPart, center);
				} else {
					// Move just the right index to the center
					findSmallestPalindrome(word, begin, end - 1, leftPart, center);
				}
			}
		} else if (begin == end - 1) {
			char c = word.charAt(begin);
			center.append(c);
		}
	}
	
	public static void main(String[] args) {
		String[] words = new String[] {"ba", "lov", "ovol", "Brilliantforever"};
		
		Palindrome p = new Palindrome();
		
		for (String word : words) {
			String result = p.findSmallestPalindrome(word);
			System.out.printf("%s -> %s (length: %d)\n", word, result, result.length());
		}
	}
}